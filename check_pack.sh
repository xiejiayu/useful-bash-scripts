#!/bin/bash
# Purpose: Check if a package is installed and the version of it
# If not, it will install the package
# Author: xie.jiayu@MU
# Last updated on : 24-Jun-2021
# -----------------------------------------------

# testing:
# pkg="ros_melodic_moveit"
read -p "Enter name of package: " pkg
if dpkg --get-selections | grep -q "^$pkg[[:space:]]*install$" >/dev/null;
then
	echo "$pkg already installed"
	echo $(dpkg-query -W -f='${Status} ${Version}\n' $pkg)
else
	echo "$pkg NOT installed"
	if apt-get -qq install $pkg; then
	    echo "Successfully installed $pkg"
	else
	    echo "Error installing $pkg"
	fi
fi
